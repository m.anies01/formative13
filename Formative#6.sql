select 	p.name_product as 'Nama Produk', 
		m.name_manufacturer as 'Nama Manufakturer', 
        almt.city as 'Kota Manufakturer', 
        almt.country as 'Negara Manufakturer',
		b.name_brand as 'Nama Brand',
        hrg.amount as 'Harga',
        v.code_valuta as 'Kode Valuta',
        v.name_valuta as 'Nama Mata Uang'
from product p 
	join manufacturer m 
	on p.manufacturerId = m.id_manufacturer 
    join brand b 
    on p.brandId = b.id_brand
    join address almt
    on m.addressId = almt.idAddress
    join price hrg 
    on hrg.productId = p.id_product
    join valuta v
    on hrg.valutaId = v.id_valuta
    group by v.code_valuta
    order by hrg.amount desc;