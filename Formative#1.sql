show databases;

create database formative13;

use formative13;

show tables;

create table address(
	idAddress int not null auto_increment primary key,
    street varchar(50) not null,
    city varchar(50) not null,
    province varchar(50) not null,
    country varchar(50) not null,
    zipCode int not null
    )engine = InnoDB;

describe address;

create table manufacturer(
	id_manufacturer int not null auto_increment primary key,
    name_manufacturer varchar(50),
    addressId int,
    foreign key(addressId) references address(idAddress)
    )engine = InnoDB;

describe manufacturer;

create table brand(
	id_brand int not null auto_increment primary key,
    name_brand varchar(50)
    )engine = InnoDB;
    
describe brand;

create table product(
	id_product int not null auto_increment primary key,
    art_number varchar(50) not null,
    name_product varchar(50) not null,
    description text not null,
    manufacturerId int not null,
    brandId int not null,
    stock int unsigned not null default 0,
    foreign key(manufacturerId) references manufacturer(id_manufacturer),
    foreign key(brandId) references brand(id_brand) 
    )engine = InnoDB;

describe product;

create table valuta(
	id_valuta int not null auto_increment primary key,
    code_valuta varchar(10) not null,
    name_valuta varchar(50) not null
    )engine = InnoDB;
    
describe valuta;

create table price(
	id_price int not null auto_increment primary key,
    productId int not null,
    valutaId int not null,
    amount int unsigned not null default 0,
    foreign key(productId) references product(id_product),
    foreign key(valutaId) references valuta(id_valuta)
    )engine = InnoDB;

describe price;