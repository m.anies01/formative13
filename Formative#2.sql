insert into address(
	idAddress,
    street,
    city,
    province,
    country,
    zipCode)
    values (1,
    'Banowati Selatan V',
    'Semarang',
    'Jawa Tengah',
    'Indonesia',
    59581),
    (2,
    'Jl. Kopeng Raya',
    'Salatiga',
    'Jawa Tengah',
    'Indonesia',
    59511);
    
select * from address;

insert into manufacturer(
	id_manufacturer,
    name_manufacturer,
    addressId)
    values (1,
    'Jamu Njonja Meneer',
    2),
    (2,
    'Polytron',
    1);
    
select * from manufacturer;

insert into brand (
	id_brand,
    name_brand)
    values (1,
    'Buyung Upik'),
    (2,
    'Polytron');
    
insert into product (
	id_product,
    art_number,
    name_product,
    description,
    manufacturerId,
    brandId,
    stock) 
    values 	(1,'JM001','Jamu Pegel Linu','Mengatasi badan pegal-pegal',1,1,30),
			(2,'JM002','Jamu Flu Tulang','Mengatasi nyeri sendi',1,1,13),
            (3,'JM003','Jamu Napsu Makan','Menambah nafsu makan pada anak',1,1,35),
            (4,'JM004','Jamu Sari Singset','Membuat tubuh menjadi langsing',1,1,0),
            (5,'JM005','Jamu Klisir','Untuk memperkecil pembengkakan akibat radang amandel',1,1,5),
            (6,'EL001','Kulkas 1 Pintu','Kulkas dengan 1 pintu, hemat energi',2,2,0),
            (7,'EL002','Kulkas 2 Pintu','Kulkas dengan 2 pintu, lebih cepat mendinginkan dan kapasitas lebih banyak',2,2,36),
            (8,'EL003','Mesin Cuci Otomatis','Dengan pengering otomatis',2,2,23),
            (9,'EL004','LCD TV 60inch','Layar yang besar dengan resolusi 4K menonton menjadi lebih nyata',2,2,89),
            (10,'EL005','AC 10pk','AC dengan teknologi mutakhir lebih hemat energi',2,2,15);
            
select * from product;

insert into valuta (
	id_valuta,
    code_valuta,
    name_valuta)
    values 	(1,'idr','Rupiah'),
			(2,'usd','US Dollar');

select * from valuta;

insert into price (
	id_price,
    productId,
    valutaId,
    amount )
    values 	(1,1,1,15000),
			(2,1,2,1),
            (3,2,1,18000),
            (4,2,2,2),
            (5,3,1,26000),
            (6,3,2,3),
            (7,4,1,14000),
            (8,4,2,1),
            (9,5,1,19500),
            (10,5,2,2),
            (11,6,1,3500000),
			(12,6,2,248),
            (13,7,1,4000000),
            (14,7,2,283),
            (15,8,1,2800000),
            (16,8,2,198),
            (17,9,1,1950000),
            (18,9,2,138),
            (19,10,1,2300000),
            (20,10,2,163);

select * from price;